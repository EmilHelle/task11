﻿using ConsoleApp1.paymentMethods.cash;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public class VendingMachine
    {
        private string paymentMethodChoice;
        private string productChoice;
        private int insertedCardPayment;
        private List<Coin> insertedCoins;
        private List<Note> insertedNotes;

        public VendingMachine()
        {
            this.insertedCardPayment = 0;
            this.insertedCoins = new List<Coin>();
            this.insertedNotes = new List<Note>();
        }

        public string PaymentMethodChoice { get => paymentMethodChoice; set => paymentMethodChoice = value; }
        public string ProductChoice { get => productChoice; set => productChoice = value; }
        public int InsertedCardPayment { get => insertedCardPayment; set => insertedCardPayment = value; }

        public void ChoosePaymentMethod(string paymentMethod)
        {
            if (AcceptablePaymentMethod(paymentMethod))
            {
                PaymentMethodChoice = paymentMethod;
                Console.WriteLine("You chose " + PaymentMethodChoice + " as your payment method");
            }
            else { Console.WriteLine("Payment method chosen is not acceptable"); }
        }

        public void ChooseProductChoice(string productChoice)
        {
            if (AcceptableProductChoice(productChoice))
            {
                ProductChoice = productChoice;
                Console.WriteLine("You chose " + ProductChoice + " as the product you want to buy");
            }
            else { Console.WriteLine("Product chosen is not acceptable"); }
        }

        public bool PaymentMethodChosen()
        {
            if (PaymentMethodChoice != "" || PaymentMethodChoice != null)
            {
                return true;
            }
            else { return false; }
        }

        public bool ProductChosen()
        {
            if (ProductChoice != "" || ProductChoice != null)
            {
                return true;
            }
            else { return false; }
        }

        //does not calculate realistically, rest of the program became too ambitious
        //
        public List<Coin> GetCoinsChange()
        {
            if (this.productChoice == "soda" || this.productChoice == "Soda")
            {
                int productPrice = 20;
                List<Coin> change = this.insertedCoins.GetRange(0, this.insertedCoins.Count - 2);
                this.insertedCoins.RemoveRange(0, 2);
                return change;
            }
            else if (this.productChoice == "snickers" || this.productChoice == "Snickers")
            {
                int productPrice = 40;
                List<Coin> change = this.insertedCoins.GetRange(0, this.insertedCoins.Count - 4);
                this.insertedCoins.RemoveRange(0, 4);
                return change;
            }
            else
            {
                Console.WriteLine("Something went wrong, you get back your money");
                return this.insertedCoins;
            }
        }

        public int GetChange()
        {
            if (this.productChoice == "soda" || this.productChoice == "Soda")
            {
                int productPrice = 20;
               int change = this.insertedCardPayment - productPrice;
                this.insertedCardPayment = 0;
                return change;
            }
            else if (this.productChoice == "snickers" || this.productChoice == "Snickers")
            {
                int productPrice = 40;
                int change = this.insertedCardPayment - productPrice;
                this.insertedCardPayment = 0;
                return change;
            }
            else
            {
                Console.WriteLine("Something went wrong, you get back your money");
                return this.insertedCardPayment;
            }
        }

        public void ReceivePayment(int payment)
        {
            if (this.insertedCardPayment == 0)
            {
                this.insertedCardPayment = payment;
                Console.WriteLine("Received payment: " + this.insertedCardPayment + " in vending machine");
            }
            else { Console.WriteLine("Already received payment"); }
        }

        public void ReceivePayment(List<Coin> coins)
        {
            if (this.insertedCoins.Count == 0)
            {
                this.insertedCoins = coins;
                Console.WriteLine("Received " + this.insertedCoins.Count + " coins in vending machine");
            }
            else { Console.WriteLine("Already received coins"); }
        }

        public bool PaymentReceived()
        {
            if (this.InsertedCardPayment != 0)
            {
                return true;
            }
            else { return false; }
        }

        private bool AcceptablePaymentMethod(string paymentMethod)
        {
            string cardMethod = "Card";
            string cashMethod = "Cash";
            if ((paymentMethod == cardMethod || paymentMethod == cashMethod) ||
                (paymentMethod == cardMethod.ToLower() || paymentMethod == cashMethod.ToLower()))
            {
                return true;
            }
            else { return false; }
        }

        private bool AcceptableProductChoice(string productChoice)
        {
            string soda = "Soda";
            string snickers = "Snickers";
            if ((productChoice == soda || productChoice == snickers) ||
                (productChoice == soda.ToLower() || productChoice == snickers.ToLower()))
            {
                return true;
            }
            else { return false; }
        }
    }
}

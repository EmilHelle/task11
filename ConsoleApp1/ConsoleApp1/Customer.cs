﻿using ConsoleApp1.paymentMethods.cash;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Customer
    {
        private string name;
        private Card card;
        private Cash cash;

        public Customer(string name, Card card, Cash cash)
        {
            this.name = name;
            this.card = card;
            this.cash = cash;
        }

        public string Name { get => name; set => name = value; }
        public Card Card { get => card; set => card = value; }
        internal Cash Cash { get => cash; set => cash = value; }
    }
}

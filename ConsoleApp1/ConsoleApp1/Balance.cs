﻿using System;

namespace ConsoleApp1
{
    public class Balance
    {
        private int amount;

        public Balance()
        {
        }

        public Balance(int amount)
        {
            this.amount = amount;
        }

        public int Amount { get => amount; set => amount = value; }

        public void Withdraw(int amount)
        {
            if (Amount < amount)
            {
                Console.WriteLine("Amount too high");
            }
            else
            {
                Amount -= amount;
            }
        }
        public void Deposit(int amount)
        {
            this.amount += amount;
        }
    }
}

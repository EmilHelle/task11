﻿namespace ConsoleApp1
{
   public abstract class Card
    {
        private int number;
        private int pin;
        private Balance balance;

       public Card(int number, int pin, Balance balance)
        {
            this.number = number;
            this.pin = pin;
            this.balance = balance;
        }

        public int Number { get => number; set => number = value; }
        public int Pin { get => pin; set => pin = value; }
        public Balance Balance { get => balance; set => balance = value; }
    }
}

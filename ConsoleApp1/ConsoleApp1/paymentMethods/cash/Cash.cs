﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1.paymentMethods.cash
{
    public class Cash
    {
        private Balance balance;
        private List<Coin> coins;
        private List<Note> notes;

        public Cash(List<Coin> coins, List<Note> notes)
        {
            this.balance = new Balance();
            this.coins = coins;
            this.notes = notes;
            this.UpdateBalance();
        }

        public Balance Balance { get => balance; set => balance = value; }
        public List<Coin> Coins { get => coins; set => coins = value; }
        public List<Note> Notes { get => notes; set => notes = value; }

        public void UpdateBalance()
        {
            this.Balance.Amount = 0;
            this.balance.Amount = this.CoinsAmount() + this.NotesAmount();
            Console.WriteLine("Cash Balance is now: " + this.Balance.Amount);
        }

        public void RemoveCoins(int numberOfCoins)
        {
            this.Coins.RemoveRange(0, numberOfCoins);
            this.UpdateBalance();
        }

        public List<Coin> GetCoins(int numberOfCoins)
        {
            List<Coin> coins = this.Coins.GetRange(0, numberOfCoins);
            this.UpdateBalance();
            return coins;
        }

        public int CoinsAmount()
        {
            int totalAmount = 0;
            foreach (Coin coin in coins)
            {
                totalAmount += coin.Value;
            }
            return totalAmount;
        }

        public int NotesAmount()
        {
            int totalAmount = 0;
            foreach (Note note in notes)
            {
                totalAmount += note.Value;
            }
            return totalAmount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.paymentMethods.cash
{
    public class Note
    {
        private int value;

        public Note(int value)
        {
            this.value = value;
        }

        public int Value { get => value; set => this.value = value; }
    }
}

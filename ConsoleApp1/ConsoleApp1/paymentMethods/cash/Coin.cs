﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.paymentMethods.cash
{
    public class Coin
    {
        private int value;

        public Coin(int value)
        {
            this.value = value;
        }

        public int Value { get => value; set => this.value = value; }
    }
}

﻿using ConsoleApp1.paymentMethods.cash;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            //instantiate customer
            Customer customer = new Customer(
                "customerName",
                new CreditCard(12353, 4523, new Balance(100)),
                new Cash(new List<Coin>() {
                new Coin(10),
                new Coin(10),
                new Coin(10),
                new Coin(10),
                 new Coin(10)
                },
                new List<Note>() {
                new Note(100),
                new Note(100)
                })
                );

            //instantiate vending machine
            VendingMachine vendingMachine = new VendingMachine();

            int payment;
            bool success;

            Console.WriteLine("Hello Customer! Please choose your payment method");
            Console.WriteLine("Choose between card or cash: ");
            string paymentMethod = Console.ReadLine();

            //choose payment method
            vendingMachine.ChoosePaymentMethod(paymentMethod);

            //choose product
            if (vendingMachine.PaymentMethodChosen())
            {
                Console.WriteLine("Please choose between soda or snickers: ");
                string productChoice = Console.ReadLine();
                vendingMachine.ChooseProductChoice(productChoice);
            }

            //insert payment
            if (vendingMachine.ProductChosen())
            {
                do
                {
                    if (vendingMachine.PaymentMethodChoice == "card")
                    {
                        Console.WriteLine("Please insert payment: ");
                        success = int.TryParse(Console.ReadLine(), out payment);
                    }
                    else
                    {
                        //I stuck with coins otherwise the program would have been
                        //bloated and could not find a good change calculation in time
                        Console.WriteLine("You have " +
                            customer.Cash.Coins.Count +
                            " coins, please insert a number of those coins: ");
                        success = int.TryParse(Console.ReadLine(), out payment);
                    }

                } while (!success);

                //pay and return change
                if (vendingMachine.PaymentMethodChoice == "card")
                {
                    vendingMachine.ReceivePayment(payment);
                    customer.Card.Balance.Withdraw(payment);
                    Console.WriteLine("Returned change: " + vendingMachine.GetChange());
                }
                else if (vendingMachine.PaymentMethodChoice == "cash")
                {
                    vendingMachine.ReceivePayment(customer.Cash.GetCoins(payment));
                    customer.Cash.RemoveCoins(payment);
                    Console.WriteLine("Returned change: " + vendingMachine.GetCoinsChange().Count + " coins");
                }

            }
        }
    }
}
